import {
  useWatchForm,
  useAllFormFields,
  useField,
} from "payload/components/forms";
import { useLocale, useConfig } from "payload/components/utilities";
import React, { useEffect, useState, useCallback } from "react";
import UploadInput from "payload/dist/admin/components/forms/field-types/Upload/Input";
import { Props as UploadFieldType } from "payload/dist/admin/components/forms/field-types/Upload/types";
import { Field } from "payload/dist/admin/components/forms/Form/types";
import { PluginConfig } from "../types";
const noImage = 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTAyNCIgaGVpZ2h0PSIxMDI0IiB2aWV3Qm94PSIwIDAgMTAyNCAxMDI0IiBmaWxsPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8cmVjdCB3aWR0aD0iMTAyNCIgaGVpZ2h0PSIxMDI0IiBmaWxsPSIjODg4ODg4IiBmaWxsLW9wYWNpdHk9IjAuMSIvPgo8cGF0aCBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGNsaXAtcnVsZT0iZXZlbm9kZCIgZD0iTTMzMiA0MDVDMzMyIDM4OC40MzEgMzQ1LjQzMSAzNzUgMzYyIDM3NUg2NjJDNjc4LjU2OSAzNzUgNjkyIDM4OC40MzEgNjkyIDQwNVY2MTlDNjkyIDYzNS41NjkgNjc4LjU2OSA2NDkgNjYyIDY0OUgzNjJDMzQ1LjQzMSA2NDkgMzMyIDYzNS41NjkgMzMyIDYxOVY0MDVaTTQ1MiA0NTVDNDUyIDQ3Ny4wOTEgNDM0LjA5MSA0OTUgNDEyIDQ5NUMzODkuOTA5IDQ5NSAzNzIgNDc3LjA5MSAzNzIgNDU1QzM3MiA0MzIuOTA5IDM4OS45MDkgNDE1IDQxMiA0MTVDNDM0LjA5MSA0MTUgNDUyIDQzMi45MDkgNDUyIDQ1NVpNMzkzIDU5OUg2MzAuNUM2MzcuMTI3IDU5OSA2NDIuNSA1OTMuNjI3IDY0Mi41IDU4N1Y1MjguOTcxQzY0Mi41IDUyNS43ODggNjQxLjIzNiA1MjIuNzM2IDYzOC45ODUgNTIwLjQ4NUw1ODQuOTg1IDQ2Ni40ODVDNTgwLjI5OSA0NjEuNzk5IDU3Mi43MDEgNDYxLjc5OSA1NjguMDE1IDQ2Ni40ODVMNDg3LjQ4NSA1NDcuMDE1QzQ4Mi43OTkgNTUxLjcwMSA0NzUuMjAxIDU1MS43MDEgNDcwLjUxNSA1NDcuMDE1TDQ0My45ODUgNTIwLjQ4NUM0MzkuMjk5IDUxNS43OTkgNDMxLjcwMSA1MTUuNzk5IDQyNy4wMTUgNTIwLjQ4NUwzODQuNTE1IDU2Mi45ODVDMzgyLjI2NCA1NjUuMjM2IDM4MSA1NjguMjg4IDM4MSA1NzEuNDcxVjU4N0MzODEgNTkzLjYyNyAzODYuMzczIDU5OSAzOTMgNTk5WiIgZmlsbD0iIzg4ODg4OCIgZmlsbC1vcGFjaXR5PSIwLjIiLz4KPC9zdmc+Cg==';
import {
  FieldType,
  Options,
} from "payload/dist/admin/components/forms/useField/types";

type PreviewFieldWithProps = UploadFieldType &
  Field & {
    pluginConfig: PluginConfig;
    path: string;
  };
export const Preview: React.FC<PreviewFieldWithProps | {}> = (props) => {
  const {
    pluginConfig: { generateURL, generateImage },
    label,
    fieldTypes,
    name,
    pluginConfig,
  } = (props as PreviewFieldWithProps) || {}; // TODO: this typing is temporary until payload types are updated for custom field props;
  const field: FieldType<string> = useField(props as Options);
  const relationTo = pluginConfig?.uploadsCollection ?? "";
  // const { value, setValue, showError } = field;
  // const { fields } = useWatchForm();
  const [value,setValue]=useState("");
  const [fields] = useAllFormFields();
  const locale = useLocale();

  const config = useConfig();

  const { collections, serverURL, routes: { api } = {} } = config;
  const collection =
    collections?.find((coll) => coll.slug === relationTo) || undefined;

  const {
    "meta.title": { value: metaTitle } = {} as Field,
    "meta.description": { value: metaDescription } = {} as Field,
    "meta.image": { value: metaThumbnail } = {} as Field,
  } = fields;

  const [href, setHref] = useState<string>();
  // console.log(metaThumbnail,'metaThumbnail');
  const fetchFile = async () => {
    if(metaThumbnail){
      const response = await fetch(`${serverURL}/api/${relationTo}/${metaThumbnail}`, {
      credentials: "include",
      });
      if (response.ok) {
        const json = await response.json();
        setValue(json?.imageurl??json?.url);
      }
    }
  };
  useEffect(() => {
    fetchFile();
  }, [metaThumbnail]);

  useEffect(() => {
    const getHref = async () => {
      if (typeof generateURL === "function" && !href) {
        const newHref = await generateURL({ doc: { fields } });
        setHref(newHref);
      }
    };
    getHref();
  }, [generateURL, fields]);
  const hasImage = Boolean(value);
if(!hasImage){
  fetchFile();
}
  return (
    <div>
      <div>Preview</div>
      <div
        style={{
          marginBottom: "5px",
          color: "#9A9A9A",
        }}
      >
        Exact result listings may vary based on content and search relevancy.
      </div>
      <div
        style={{
          padding: "0px",
          borderRadius: "5px",
          boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
          pointerEvents: "none",
          // maxWidth: "600px",
          width: "100%",
          background: "var(--theme-elevation-50)",
          display: "flex",
        }}
        className="file-details"
      >
        <div
          style={{
          }}
          className="thumbnail thumbnail--size-medium"
        >
          <img
            src={hasImage?value:noImage}
            style={{
              marginBottom: 0,
            }}
          />
        </div>
        <div
          className="file-details__main-detail"
        >
          <div>
            <a
              href={href}
              style={{
                textDecoration: "none",
              }}
            >
              {href || "https://..."}
            </a>
          </div>
          <h4
            style={{
              margin: 0,
            }}
          >
            <a
              href="/"
              style={{
                textDecoration: "none",
              }}
            >
              {metaTitle as string}
            </a>
          </h4>
          <p
            style={{
              margin: 0,
            }}
          >
            {metaDescription as string}
          </p>
        </div>
      </div>
    </div>
  );
};

export const getPreviewField = (props: any) => <Preview {...props} />;
