export const defaults = {
  title: {
    minLength: 50,
    maxLength: 150,
  },
  description: {
    minLength: 100,
    maxLength: 250,
  },
};
